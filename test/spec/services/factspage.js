'use strict';

describe('Service: factspage', function () {

  // load the service's module
  beforeEach(module('aufiApp'));

  // instantiate service
  var factspage;
  beforeEach(inject(function(_factspage_) {
    factspage = _factspage_;
  }));

  it('should do something', function () {
    expect(!!factspage).toBe(true);
  });

});
