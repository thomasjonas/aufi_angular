'use strict';

describe('Controller: FactsmenuCtrl', function () {

  // load the controller's module
  beforeEach(module('aufiApp'));

  var FactsmenuCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FactsmenuCtrl = $controller('FactsmenuCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
