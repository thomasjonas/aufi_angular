'use strict';

angular.module('_', [])
  .factory('_', function () {
    return window._;
  });
