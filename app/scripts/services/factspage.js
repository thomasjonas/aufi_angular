'use strict';

angular.module('aufiApp')
	.factory('factspage', function($resource, _, baseUrl) {
		this.items = [];
		this.levels = [];
		this.currentChildren;

		return {
			main: this.items,
			levels: this.levels,

			addLevel: function(parent, level) {
				this.currentChildren = parent.children;

				if (angular.isDefined(parent.slug)) {
					this.slug += "/" + parent.slug;
				}

				var newLevel = {
					parent: parent,
					level: level,
					slug: this.slug,
					children: parent.children
				}			

				this.levels[level-1] = newLevel;
			},

			addSlug: function(slug, level) {
				if (this.levels.length > level) {
					this.levels = this.levels.slice(0, level);

					if (level == 0) {
						this.slug = "/facts";
						this.currentChildren = this.items;
					} else {
						this.slug = this.levels[level-1].slug;
						this.currentChildren = this.levels[level-1].children;
					}
				}

				var item = _.findWhere(this.currentChildren, {slug: slug});
				this.addLevel(item, level + 1);
			},

			processSlug: function(slug) {
				this.reset();

				var levels = slug.split("/");
				_.each(levels, function(s) {
					var item = _.findWhere(this.currentChildren, {slug: s});
					this.addLevel(item, this.levels.length + 1);
				}, this);

			},

			loadMenu: function(callback) {
				this.items = $resource(baseUrl + '/facts/menu').query(callback);
				this.reset();
			},

			// loadFacts: function() {
			// 	$resource(baseUrl + '/facts/menu').get({id: 10});
			// },

			reset: function() {
				this.currentPage = '';
				this.currentLevel = 0;
				this.levels = [];
				this.currentChildren = this.items;
				this.main = this.items;
				this.currentChildren = this.items;
				this.slug = "/facts";
			}
		}
	});
