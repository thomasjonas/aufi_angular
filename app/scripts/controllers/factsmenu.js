'use strict';

angular.module('aufiApp')
	.controller('FactsmenuCtrl', function ($scope, factspage, $routeParams, $location) {

		if (angular.isUndefined(factspage.items)) {
			factspage.loadMenu(function(){
				var slug = $scope.getSlug();
				if (angular.isDefined(slug)) {
					factspage.processSlug(slug);
				}
			});
		}

		$scope.menu = factspage;

		$scope.showSubmenu = function(parent, level) {
			factspage.addLevel(parent, level);
		}

		$scope.menuTitle = function(item) {
			if (item.short_title !== '') {
				return item.short_title;
			} else {
				return item.title;
			}
		}

		$scope.addSlug = function(slug, level) {
			factspage.addSlug(slug, level);
		}

		$scope.getSlug = function() {
			var params = $routeParams.menu;
			if (angular.isDefined($routeParams.sub)) params += $routeParams.sub;
			//factspage.processSlug(params);
			return params;
		}
	});
