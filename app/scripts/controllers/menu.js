'use strict';

angular.module('aufiApp')
  .controller('MenuCtrl', function ($scope, $location) {
    $scope.routeIs = function (routeName) {
    	return routeName === $location.path()
    }
  });
