'use strict';

var app = angular.module('aufiApp', ['ngResource', 'ngRoute', '_'])
  .config(function ($routeProvider, $locationProvider, $httpProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $locationProvider.html5Mode(true);

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/facts/:menu?:sub*', {
        templateUrl: 'views/facts.html',
        controller: 'FactsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

app.constant("baseUrl","http://api.aufi.dev:9090\:9090");